let XLSX = require('xlsx');
let fs = require('fs');

fs.readdir(process.argv[2] + '/', function (err, files) {
    if (err) return console.error(err);
    else {
        console.log(`Started......`);
        files.forEach(function (file, index) {
            if (file.split(".")[1] == 'xlsx') {
                let workbook = XLSX.readFile(process.argv[2] + '/' + file);
                fs.writeFile(process.argv[2] + '/' + file.split(".xlsx")[0] + '.json', JSON.stringify(XLSX.utils.sheet_to_json(workbook.Sheets[workbook.SheetNames[0]]), null, 4), function (err) {
                    if (err) throw err;
                    else if (files.length - 1 == index) console.log(`Files have been converted successfully at ${process.argv[2]}`);
                }
                );
            } else console.log(`${file} is not a valid file`);
        })
    }
})